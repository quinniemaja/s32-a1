const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const port = process.env.PORT || 4000;
const userRoutes = require('./routes/userRoutes');
const courseRoutes = require('./routes/courseRoutes')

const app = express();

app.use(express.json())
app.use(express.urlencoded({extended: true}));
app.use(cors());

mongoose.connect('mongodb+srv://admin:admin131@bootcamp.y9mz6.mongodb.net/course-booking?retryWrites=true&w=majority', {

		useNewUrlParser: true,
		useUnifiedTopology: true

});

let db = mongoose.connection;

	db.on("error", console.error.bind(console, "Conncection Error"))
	db.once('open', () => console.log('Connected to MongoDB Atlas'));


app.use('/user', userRoutes);
app.use('/courses', courseRoutes);

app.listen(port, () => console.log(`Server is running at port: ${port}.`))

/*
	herokuapp
	https://infinite-savannah-10247.herokuapp.com/ | https://git.heroku.com/infinite-savannah-10247.git
*/