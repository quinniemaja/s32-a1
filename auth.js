const jwt = require('jsonwebtoken');
const secret = "CourseBookingAPI";

module.exports.createAccessToken = user => {
	//the data will be received from the registration form
	//when user logs in, a token will be created with user's information 
	const data = {
		id: user._id,
		email: user.mail,
		isAdmin: user.isAdmin
	}
	// Generate a JSON web token using the jwt's sign method
	// Generates the token using the form data, and the secret code with no additional options provided. 
	return jwt.sign(data, secret, {})
}

module.exports.verify = (req, res, next) => {
	// the token is retrieve from request header
	let token = req.headers.authorization;

	// token received and not undefined 
	if(typeof token !== undefined) {
		console.log(token)

		// *7 to slice the "bearer " word on token 
		// ex. output = Bearer djapvihvawe.ygoawe3if
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err, data) => {
			if(err) {
				return res.send({auth: "failed"})
			} else {
				next()
			}
		})
	}else {
		return res.send({auth: "failed"})
	}

}

module.exports.decode = (token) => {
	if (typeof token !== "undefined") {
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if(err) {
				return null
			} else {
				return jwt.decode(token, {complete: true}).payload
			}
		})
	}else {
		return null
	}
}