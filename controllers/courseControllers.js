const Course = require('../models/Course');
const auth = require('../auth');

module.exports.addCourse = (reqBody) => { 
			
			let newCourse = new Course({

			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		})

		return newCourse.save().then((course, err) => {
			if(err) {
				console.log(err)
				return false
			} else {
				return course
			}
		})
	}
	
	// retrieve all data 

	module.exports.getAllCourses = () => {

		return Course.find({}).then(result => {
			return result;
		})
	};

// retrieve all active courses

module.exports.getAllActive = () => {

	return Course.find({isActive: true}).then(result => {
		return result;
	})
};

// retrieve a specific course

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		let getCourse = {
			name: result.name,
			price: result.price
		}
		return getCourse
	})
}

// update a course
module.exports.updateCourse = (reqParams, reqBody) => {

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	// findByIdAndUpdate(docId, updatesToBeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, err) => {
		if(err) {
			console.log(err)
			return false
		} else {
			return course
		}
	
	})
}

// acitvity ===============================
module.exports.archivedCourse = (reqParams, reqBody) => {

		let inActiveCourse ={
			isActive: reqBody.isActive
		}
	
		return Course.findByIdAndUpdate(reqParams.courseId, inActiveCourse).then((course, err) => {

			if(err){
				console.log(err)
				return false
			} else {
				return course
			}
		})
	}
	




