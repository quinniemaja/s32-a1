const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const Course = require('../models/Course');

module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {

		if(result.length > 0) {
			return true
		} else {
			return false
		}
	})
};

module.exports.registerUser = reqBody => {

		let newUser = new User({

			firstName: reqBody.firstName,
			lastName: reqBody.lastName,
			email: reqBody.email,
			mobileNo: reqBody.mobileNo,
			password: bcrypt.hashSync(reqBody.password, 10),
			//bcrypt.hashSync(<string to be hashed>, saltRounds)

		});

		return newUser.save().then((user, err) => {
			if(err) {
				console.log(err)
				return false
			} else {
				return true
			}
		}) 

}

module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result === null) {
			return false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect) {
				return { access: auth.createAccessToken(result)}

			} else{
				return false
			}
		}
	})
}

module.exports.getProfile = (reqBody) => {

	return User.findById(reqBody.id).then((result, err) => {

		if(result === null) {
			return false
		} else {	
			result.password = ""
			return result
		}
	})
}

// for enrolling a user
// async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user. 

/*
	Using the await keyword will allow the enroll method to complete updating the user before returning a response back
*/
module.exports.enroll = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.enrollments.push({courseId: data.courseId});
		return user.save().then((user, err) => {
			if(err){
				console.log(err)
				return false
			} else {
				return true
			}
		})
	})

	let isCourseUpdated = await Course.findById(data.courseId).then((course) => {
		course.enrollees.push({userId: data.userId});
		return course.save().then((course, err) => {
			if(err){
				console.log(err)
				return false
			} else {
				return true
			}
		})
	})

	if(isUserUpdated && isCourseUpdated) {
		return true
	}else {
		return false
	}

}