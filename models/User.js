const mongoose = require('mongoose');

const userSchema = new mongoose.Schema(
	{
		firstName: {
			type: String,
			required: [true, 'Firstname is required']
		},

		lastName: {
			type: String,
			required: [true, 'Lastname is required']
		},

		email: {
			type: String,
			required: [true, 'Enter a valid Email']
		},

		password: {
			type: String,
			required: [true, 'Enter a valid password']
		},

		isAdmin: {
			type: Boolean,
			default: false
		},

		mobileNo: {
			type: String,
			required: [true, 'Enter a valid mobile number']
		},

		enrollments: [
			{
				courseId: {
					type: String,
					required: [true, 'Course ID is required']
				},

				enrolledOn:{
					type: Date,
					default: new Date()
				},

				status: {
					type: String,
					default: "Enrolled"
				}
			}]
	});

module.exports = mongoose.model('User', userSchema);