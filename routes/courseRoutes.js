const express = require('express');
const auth = require('../auth');
const router = express.Router();
const courseController = require('../controllers/courseControllers');

router.post('/', auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin !== true) {
		return res.send('Access Denied!')
	} else {
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
	}
})

// retrieve all courses

router.get('/all', (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
})

// retrieve all active courses
router.get('/', (req,res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController))
})

// retrieve specific course
router.get('/:courseId', (req,res) => {
	console.log(req.params.courseId);
	console.log(req.params);

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})

// update a course
router.put('/:courseId', auth.verify, (req, res) => {

	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})

// activity ==============================================

router.put('/:courseId/archive', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin !== true) {
		return ('Access Denied!')
	} else {
		courseController.archivedCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
	}	
	
})
module.exports = router;