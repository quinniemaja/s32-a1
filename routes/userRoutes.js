const express = require('express');

const auth = require('../auth')

const router = express.Router();

const userController = require('../controllers/userControllers')

router.post('/checkEmail', (req, res) => {

	userController.checkEmailExists(req.body).then(resultFromcontroller => 
		res.send(resultFromcontroller))
})

router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(resultFromcontroller => res.send(resultFromcontroller))
})

router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(resultFromcontroller => res.send(resultFromcontroller))
})

router.post('/details', auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization)

	userController.getProfile(req.body).then(resultFromcontroller => res.send(resultFromcontroller))
})

// for enrolling a user
router.post('/enroll', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	let data = {
		userId: userData.id,
		courseId: req.body.courseId
	}

	if(userData.isAdmin == false) {

		userController.enroll(data).then(resultFromcontroller => res.send(resultFromcontroller))
	} else {
		return res.send('Access Denied!')
	}

	

})

module.exports = router;